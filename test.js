const crypto = require("crypto");

function sign(path, secret) {
  const hash = crypto
    .createHmac("sha1", secret)
    .update(path)
    .digest("base64")
    .replace(/\+/g, "-")
    .replace(/\//g, "_");
  return hash + "/" + path;
}

console.log(sign("166x169/test.png", "mysecret"));
